from loguru import logger
import os
import sys
import time

ROTATION_TIME = "00:00"
# 日志默认保存三天
RETENTION_TIME = "72h"


class Logger:
    def __init__(self, log_dir="logs", debug=False):
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        log_name = time.strftime("%Y-%m-%d", time.localtime(time.time()))
        log_file_path = os.path.join(log_dir, log_name + '.log')

        # 移除默认的日志配置
        logger.remove()

        level = "DEBUG" if debug else "INFO"
        logger.add(sys.stdout, level=level)
        logger.add(log_file_path, rotation=ROTATION_TIME, level="DEBUG", retention=RETENTION_TIME)
        self.logger = logger
