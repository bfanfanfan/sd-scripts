# 说明

基于开源项目 [kohya-ss 的 sd-scripts](https://github.com/kohya-ss/sd-scripts)，在此基础上了增加了Gradio UI，精简了很多训练参数，适合国内初级“炼丹宝宝”体质的炼丹炉。目前只实现了LORA训练的UI。

# 安装

克隆该项目

```cmd
git clone https://gitee.com/bfanfanfan/sd-scripts.git
```

## python环境

进入项目根目录下创建python虚拟环境

```cmd
python -m venv venv
```

进入python虚拟环境，安装所需python库

```cmd
.\venv\Scripts\activate
pip install -r requirements.txt
```

安装cuda对应的torch版本，由于 kohya-ss 的 sd-scripts 项目特别说明了所需torch版本，所以必须安装以下指定torch版本，否则运行可能出错

```cmd
pip install torch==1.12.1+cu116 torchvision==0.13.1+cu116 --extra-index-url https://download.pytorch.org/whl/cu116
```

安装torch对应的xformers版本

```cmd
pip install -U -I --no-deps https://github.com/C43H66N12O12S2/stable-diffusion-webui/releases/download/f/xformers-0.0.14.dev0-cp310-cp310-win_amd64.whl
```

## bitsandbytes_windows 配置

将项目根目录下 bitsandbytes_windows 目录中的指定文件拷贝到指定python库目录下

```cmd
cp .\bitsandbytes_windows\*.dll .\venv\Lib\site-packages\bitsandbytes\
cp .\bitsandbytes_windows\cextension.py .\venv\Lib\site-packages\bitsandbytes\cextension.py
cp .\bitsandbytes_windows\main.py .\venv\Lib\site-packages\bitsandbytes\cuda_setup\main.py
```

## accelerate 配置

进入以上创建的python虚拟环境下，然后执行

```cmd
accelerate config
```

依次选择以下的值

```cmd
- This machine
- No distributed training
- NO
- NO
- NO
- 0
- fp16
```

# 运行

进入创建好的python虚拟环境下执行

```cmd
python .\main.py
```

可选 Gradio 参数

```cmd
python .\main.py [--listen 127.0.0.1] [--server_port 7860] [--share] [--username user] [--password pass]
```

最终在浏览器中访问日志中打印打链接地址看到webui，效果如下

![image-20230918224801700](./images/README/image-20230918224801700.png)

# 参考

[kohya-ss/sd-scripts](https://github.com/kohya-ss/sd-scripts)

[bmaltais/kohya_ss](https://github.com/bmaltais/kohya_ss)

[iconfont](https://www.iconfont.cn/collections/detail?spm=a313x.search_index.0.da5a778a4.6efd3a813guglU&cid=42850)